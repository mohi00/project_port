<?php
/**
 * Created by PhpStorm.
 * User: MD.MOHI UDDIN
 * Date: 2/17/2019
 * Time: 3:39 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
 protected $fillable =['uid','identifier','user_id','course_id','status','created_by','modified_by'];
}