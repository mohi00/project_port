<?php
/**
 * Created by PhpStorm.
 * User: MD.MOHI UDDIN
 * Date: 2/17/2019
 * Time: 11:45 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable =['uid','identifier','title','image','description','learning_points','course_start_date','course_end_date','course_register_date','created_by','modified_by'];
}