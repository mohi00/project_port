<?php
/**
 * Created by PhpStorm.
 * User: MD.MOHI UDDIN
 * Date: 2/17/2019
 * Time: 12:55 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Course;
use http\Env\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException as Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;



class CourseController extends Controller
{
    public function index()
    {

        $courses = Course::all();
        return response()->json($courses);

    }
    public function show($id)
    {
        try {
            $course = Course::find($id);
            return response()->json($course);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['image'] = '';
        $all_learning_points = implode (", ", $request->learning_points);
        $data['learning_points'] = $all_learning_points;

        if ($request->has('file')) {
            $requestFile = $_FILES['file']['name'];
            $explodedData = explode('.',$requestFile);
            $requestFileexptype = $explodedData[1];
            $encname =str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString()).rand(10000,99999);
            $requestFilename = $encname.'.'.$requestFileexptype;
            $data['image']  = $requestFilename;
            $requestFilepath=base_path()."/public/uploads/".$requestFilename;
            move_uploaded_file($_FILES["file"]["tmp_name"],$requestFilepath);
        }

        $course=Course::create($data);
        return response()->json($course);


    }

    public function create()
    {
        return view('Course/store');
    }


    public function edit($id)
    {
        try {
            $course = Course::find($id);
            return response()->json($course);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }





    public function update(Request $request, $id)
    {

        try{
            if ($request->has('file')) {
                $requestFile = $_FILES['file']['name'];
                $explodedData = explode('.',$requestFile);
                $requestFileexptype = $explodedData[1];
                 $encname =str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString()).rand(10000,99999);
                 $requestFilename = $encname.'.'.$requestFileexptype;
                 $request['image']  = $requestFilename;
                 $requestFilepath=base_path()."/public/uploads/".$requestFilename;

                 move_uploaded_file($_FILES['file']['tmp_name'],$requestFilepath);
                // move_uploaded_file($_FILES["image"]["tmp_name"],$requestFilepath);
            }

            $course = Course::find($id);
            $course->uid = $request->input('uid');
            $course->identifier = $request->input('identifier');
            $course->title = $request->input('title');
            $course->image = $request->input('image');
            $course->description = $request->input('description');
            $course->learning_points = $request->input('learning_points');
            $course->course_start_date = $request->input('course_start_date');
            $course->course_end_date = $request->input('course_end_date');
            $course->course_register_date = $request->input('course_register_date');
            $course->created_by = $request->input('created_by');
            $course->modified_by = $request->input('modified_by');
            $course->save();
            return response()->json($course);
        }
        catch (Exception $e){
            return \response()->json("sosoos");
        }

    }

    public function destroy($id)
    {
        try {

            $course = Course::find($id);
            $course->delete();
            return response()->json($course);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

}