<?php
/**
 * Created by PhpStorm.
 * User: MD.MOHI UDDIN
 * Date: 2/17/2019
 * Time: 3:43 PM
 */

namespace App\Http\Controllers;


use App\Enrollment;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;


class EnrollmentController extends Controller
{

    public function index()
    {

        $enrollments = Enrollment::all();
        return response()->json($enrollments);

    }
    public function show($id)
    {
        try {
            $enrollments = Enrollment::find($id);
            return response()->json($enrollments);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }
    public function store(Request $request)
    {
       try {
            $rules = array(
                "uid" => 'required',
                "identifier" => 'required',
                "user_id" => 'required',
                "course_id" => 'required',
                "status" => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $json = [
                    'success' => false,
                    'errors' => $validator->messages()
                ];
                return response()->json($json, 400);
            }
            $enrollments = Enrollment::create($request->all());

            return response()->json($enrollments);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 403);
        }
    /* $enrollments = Enrollment::create($request->all());
        // return view('Operator.store', compact('operatores'));
        return response()->json($enrollments);*/

    }
    public function create()
    {
        return view('Enrollment/store');
    }
    public function edit($id)
    {
        try {
            $enrollments = Enrollment::find($id);
            return response()->json($enrollments);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }
    public function update(Request $request, $id)
    {

        try {

            $enrollments = Enrollment::find($id);
            $enrollments->uid = $request->input('uid');
            $enrollments->identifier = $request->input('identifier');
            $enrollments->user_id = $request->input('user_id');
            $enrollments->course_id = $request->input('course_id');
            $enrollments->status = $request->input('status');
            $enrollments->created_by = $request->input('created_by');
            $enrollments->modified_by = $request->input('modified_by');
            $enrollments->save();
            return response()->json($enrollments);

        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }
    public function destroy($id)
    {
        try {

            $enrollments = Enrollment::find($id);
            $enrollments->delete();
            return response()->json($enrollments);

        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}