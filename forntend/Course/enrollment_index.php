<!doctype>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">


</head>
<body>
<ul class="nav nav-pills">
    <li class="nav-item">
        <a class="nav-link active" href="enrollment_create.php">Store data</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="enrollment_index.php">View data</a>
    </li>
</ul>
<div class="container">
    <table id="list_table" border='2' cellspocing='5' cellpading='5' class="text senter-algin">
        <thead>
        <tr bgcolor="#00ffff" cellspocing='5' cellpading='5'>
            <th>Sl NO</th>
            <th>Uid</th>
            <th>Identifier</th>
            <th>User_id</th>
            <th>Course_id</th>
            <th>Status</th>
            <th>Created_by</th>
            <th>Modified_by</th>
            <th>Created_at</th>
            <th>Update_at</th>
            <th>action</th>
        </tr>
        </thead>
        <tbody>

    </table>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Model show</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="modalForm" >
                    <div class="form-group">
                        <label for="uid">uid</label>
                        <input type="text" class="form-control" value="" name="uid" id="uid" placeholder="uid">
                    </div>
                    <div class="form-group">
                        <label for="identifier">identifier</label>
                        <input type="text" class="form-control" value="" name="identifier" id="identifier" placeholder="identifier">
                    </div>
                    <div class="form-group">
                        <label for="user_id">user_id</label>
                        <input type="text" class="form-control" value="" name="user_id" id="user_id" placeholder="user_id">
                    </div>
                    <div class="form-group">
                        <label for="course_id">course_id</label>
                        <input type="text" class="form-control" value="" name="course_id" id="course_id" placeholder="course_id">
                    </div>
                    <div class="form-group">
                        <label for="status">status</label>
                        <input type="text" class="form-control" value="" name="status" id="status" placeholder="description">
                    </div>
                    <div class="form-group">
                        <label for="created_by">created_by</label>
                        <input type="text" id="created_by" value="" name="created_by" class="form-control" placeholder="created_by" >
                    </div>
                    <div class="form-group">
                        <label for="modified_by">modified_by</label>
                        <input type="text" id="modified_by" value="" name="modified_by" class="form-control" placeholder="modified_by" >
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-defult" data-dismiss="modal">close</button>
                        <input type="text" id="enrollment_id" value="" hidden>
                        <button id="update" type="submit" class="btn btn-defult" >Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script>
    function index() {
        $.ajax({
            url : 'http://project_port.mu/enrollments',
            type : 'get',
            success : function (response) {
                let result = eval(response);
                for (let item of result){
                    let output = '<tr class="old-tr">\n' +
                        '    <td>'+item.id+'</td>\n' +
                        '    <td>'+item.uid+'</td>\n' +
                        '    <td>'+item.identifier+'</td>\n' +
                        '    <td>'+item.user_id+'</td>\n' +
                        '    <td>'+item.course_id+'</td>\n' +
                        '    <td>'+item.status+'</td>\n' +
                        '    <td>'+item.created_by+'</td>\n' +
                        '    <td>'+item.modified_by+'</td>\n' +
                        '    <td>'+item.created_at+'</td>\n' +
                        '    <td>'+item.updated_at+'</td>\n' +
                        '     <td><button id="'+item.id+'" class="btn btn-info problemshow">Show</button>|<button id="'+item.id+'" class="btn btn-info problemEdit">Edit</button>|' +
                        '       <button onclick="del('+item.id+')" class="btn btn-danger">Delete</button>'+""+'</td>\n' +

                        '</tr>';
                    $("#list_table").append(output);
                }
            }
        })
    }
    index();

    $(document).on('click', '.problemshow', (e)=>{
        $("#update").hide();
        $("#exampleModal").modal();
        show(e.target.id);
    });
    function show(id) {
        $.ajax({
            type: "get",
            url: 'http://project_port.mu/enrollments/show/' + id,
            success: (data) => {
                //$("#enrollment_id").val(data.id).prop('readonly', true);
                $("#uid").val(data.uid).prop('readonly', true);
                $("#identifier").val(data.identifier).prop('readonly', true);
                $("#user_id").val(data.user_id).prop('readonly', true);
                $("#course_id").val(data.course_id).prop('readonly', true);
                $("#status").val(data.status).prop('readonly', true);
                $("#created_by").val(data.created_by).prop('readonly', true);
                $("#modified_by").val(data.modified_by).prop('readonly', true);
            }
        });
    }

    $(document).on('click', '.problemEdit', (e) => {
        $("#update").show();
        $("#exampleModal").modal();
        edit(e.target.id);
    });

    function edit(id) {
        $.ajax({
            type: "get",
            url: 'http://project_port.mu/enrollments/edit/'+ id,
            success: (data) => {
                //console.log(data);
                $("#enrollment_id").val(data.id).prop('readonly', false);
                $("#uid").val(data.uid).prop('readonly', false);
                $("#identifier").val(data.identifier).prop('readonly', false);
                $("#user_id").val(data.user_id).prop('readonly', false);
                $("#course_id").val(data.course_id).prop('readonly', false);
                $("#status").val(data.status).prop('readonly', false);
                $("#created_by").val(data.created_by).prop('readonly', false);
                $("#modified_by").val(data.modified_by).prop('readonly', false);
            }
        });
    }
    function update() {
        let id = $("#enrollment_id").val();
        let uid = $("#uid").val();
        let identifier = $("#identifier").val();
        let user_id = $("#user_id").val();
        let course_id = $("#course_id").val();
        let status = $("#status").val();
        let created_by = $("#created_by").val();
        let modified_by = $("#modified_by").val();
        $.ajax({
            type: "put",
            data: {uid, identifier, user_id, course_id, status, created_by, modified_by},
            url: 'http://project_port.mu/enrollments/update/' + id,
            success: (data) => {
                $(".old-tr").remove();
                index();
            }
        });
    }
    $(document).on('click', '#update', (e) => {
        update();
        e.preventDefault();
    });
    function del(id) {
//        console.log(id);
        if (confirm('Aer you sure want to delete ?')) {
            $.ajax({
                type: "delete",
                url: 'http://project_port.mu/enrollments/destroy/' + id,
                success: (data) => {
                    $(".old-tr").remove();
                    index();
                }
            });
        }
        else {
            return false;
        }

    }


</script>
</body>
</html>
