<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix'=> 'courses'],function () use ($router) {
    $router->get('/', 'CourseController@index');
    $router->get('/show/{id}', 'CourseController@show');
    $router->get('/edit/{id}', 'CourseController@edit');
    $router->post('/', 'CourseController@store');
    $router->post('/update/{id}', 'CourseController@update');
    $router->delete('destroy/{id}', 'CourseController@destroy');
    //$router->destroy('delete/{id}', 'CourseController@destroy');

});
$router->group(['prefix'=> 'enrollments'],function () use ($router) {
    $router->get('/', 'EnrollmentController@index');
    $router->get('/show/{id}', 'EnrollmentController@show');
    $router->get('/edit/{id}', 'EnrollmentController@edit');
    $router->post('/store', 'EnrollmentController@store');
    $router->put('/update/{id}', 'EnrollmentController@update');
    $router->delete('/destroy/{id}', 'EnrollmentController@destroy');
});